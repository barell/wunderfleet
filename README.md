Wunder Mobility - Recruitment Task
===

_Task completed by Bartosz Tomczak_

---

To run the project locally you can use docker and docker-compose:

`docker-compose up -d`

Then open running php container shell:

`docker exec -it wunderfleet_php_1 bash`

Install dependencies:

`composer install`

And setup database schema:

`composer db:dev:reset`

Now open http://localhost/ and you should see the app.

---

Database structure is in schema.sql file.

---

Answers to questions 
===

1. Describe possible performance optimizations for your Code.
   
I think one of the optimization could be done in AppControler where it saves customer data, then fetches database to get payment ID.
That could be optimized by storing API response in cache and then use it on success page directly from cache.

2. Which things could be done better, than you’ve done it?

I think I could write some more tests to cover submitting post data, test if input is not missing etc.