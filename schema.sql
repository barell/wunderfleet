CREATE TABLE customer
(
    id              INT AUTO_INCREMENT NOT NULL,
    first_name      VARCHAR(255)       NOT NULL,
    last_name       VARCHAR(255)       NOT NULL,
    telephone       VARCHAR(30)        NOT NULL,
    street          VARCHAR(255)       NOT NULL,
    house_number    VARCHAR(30)        NOT NULL,
    zip_code        VARCHAR(30)        NOT NULL,
    city            VARCHAR(255)       NOT NULL,
    account_owner   VARCHAR(255)       NOT NULL,
    iban            VARCHAR(34)        NOT NULL,
    payment_data_id VARCHAR(255) DEFAULT NULL,
    PRIMARY KEY (id)
) DEFAULT CHARACTER SET utf8mb4
  COLLATE `utf8mb4_unicode_ci`
  ENGINE = InnoDB;
