<?php

declare(strict_types=1);

namespace App\Tests\Integration;

use App\Command\RegistrationCommand;
use App\Entity\Customer;
use App\Handler\RegistrationHandler;
use App\Tests\Double\Stub\PaymentApiStub;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Constraint\IsEqual;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class RegistrationHandlerTest extends KernelTestCase
{
    private EntityManagerInterface $entityManager;

    protected function setUp()
    {
        parent::setUp();

        self::bootKernel();

        $this->entityManager = self::$container->get('doctrine')->getManager();
    }

    public function test_registration()
    {
        $command = new RegistrationCommand(
            'John',
            'Bloggs',
            '123123',
            'Lower St',
            '123',
            '1133',
            'Tokio',
            'John Bloggs',
            '44332211'
        );

        $handler = new RegistrationHandler(
            $this->entityManager,
            new PaymentApiStub()
        );

        $handler->handle($command);

        /**
         * @var Customer $customer
         */
        $customer = $this->entityManager->getRepository(Customer::class)->find(1);

        $this->assertThat($customer->getId(), new IsEqual(1));
        $this->assertThat($customer->getPaymentDataId(), new IsEqual(1234));
    }
}
