<?php

declare(strict_types=1);

namespace App\Tests\Double\Stub;

use App\Api\PaymentApiInterface;

class PaymentApiStub implements PaymentApiInterface
{
    public function savePaymentData(int $customerId, string $iban, string $accountOwner): string
    {
        return '1234';
    }
}
