<?php

declare(strict_types=1);

namespace App\Api;

interface PaymentApiInterface
{
    public function savePaymentData(
        int $customerId,
        string $iban,
        string $accountOwner
    ): string;
}
