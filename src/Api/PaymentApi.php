<?php

declare(strict_types=1);

namespace App\Api;

final class PaymentApi implements PaymentApiInterface
{
    private JsonClient $client;

    public function __construct(JsonClient $client)
    {
        $this->client = $client;
    }

    public function savePaymentData(int $customerId, string $iban, string $accountOwner): string
    {
        $response = $this->client->request(
            'POST',
            'wunderfleet-recruiting-backend-dev-save-payment-data',
            [
                'customerId' => $customerId,
                'iban'       => $iban,
                'owner'      => $accountOwner
            ]
        );

        return (string) $response['paymentDataId'];
    }
}
