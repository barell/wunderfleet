<?php

declare(strict_types=1);

namespace App\Api;

use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;
use Webmozart\Assert\Assert;

class JsonClient
{
    private ClientInterface $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function request(string $method, string $resource, array $parameters = []): ?array
    {
        $options = [];

        if (count($parameters)) {
            $options['json'] = $parameters;
        }

        try {
            $response = $this->client->request($method, $resource, $options);
        } catch (\Throwable $e) {
            throw new \Exception(
                \sprintf('Could not perform the request when calling %s resource', $resource),
                $e->getCode(),
                $e
            );
        }

        Assert::eq(
            $response->getStatusCode(),
            Response::HTTP_OK,
            \sprintf('Received %d response code but expected 200', $response->getStatusCode())
        );

        return \json_decode($response->getBody()->getContents(), true);
    }
}
