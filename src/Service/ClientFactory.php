<?php

declare(strict_types=1);

namespace App\Service;

use App\Api\JsonClient;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

final class ClientFactory
{
    public static function createRaw(string $baseUri): ClientInterface
    {
        return new Client(
            [
                'base_uri' => $baseUri
            ]
        );
    }

    public static function createJson(string $baseUri): JsonClient
    {
        return new JsonClient(self::createRaw($baseUri));
    }
}
