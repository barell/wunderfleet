<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Log\LoggerInterface;

final class CommandBus
{
    private array $handlers = [];

    private LoggerInterface $logger;

    public function __construct(\Traversable $handlers, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $handlers = \iterator_to_array($handlers);

        foreach ($handlers as $handler) {
            $this->handlers[$handler->handles()] = $handler;
        }
    }

    public function handleCommand($command)
    {
        try {
            return $this->handleInternal($command);
        } catch (\Throwable $e) {
            $this->logger->error(
                \sprintf(
                    '%s %s',
                    $e->getMessage(),
                    $e->getPrevious() === null ? '' : $e->getPrevious()->getMessage()
                )
            );

            throw $e;
        }
    }

    private function handleInternal($command)
    {
        $commandClass = \get_class($command);

        if (isset($this->handlers[$commandClass])) {
            try {
                return $this->handlers[$commandClass]->handle($command);
            } catch (\Throwable $e) {
                throw new \Exception(
                    \sprintf(
                        'Failed to handle %s command',
                        $commandClass
                    ),
                    $e->getCode(),
                    $e
                );
            }
        } else {
            throw new \Exception(
                \sprintf(
                    'No handler found for %s command',
                    $commandClass
                )
            );
        }
    }
}
