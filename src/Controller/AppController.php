<?php

namespace App\Controller;

use App\Command\RegistrationCommand;
use App\Repository\CustomerRepository;
use App\Service\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{
    /**
     * @Route("/", name="home", methods={"GET"})
     */
    public function index(): Response
    {
        return $this->render('app/index.html.twig');
    }

    /**
     * @Route("/process", name="process", methods={"POST"})
     */
    public function process(
        Request $request,
        CommandBus $commandBus,
        CustomerRepository $customerRepository,
        SessionInterface $session
    ): RedirectResponse {
        $registrationCommand = new RegistrationCommand(
            $request->request->get('first_name'),
            $request->request->get('last_name'),
            $request->request->get('telephone'),
            $request->request->get('street'),
            $request->request->get('house_no'),
            $request->request->get('zipcode'),
            $request->request->get('city'),
            $request->request->get('account_owner'),
            $request->request->get('iban')
        );

        try {
            $id = $commandBus->handleCommand($registrationCommand);
            $customer = $customerRepository->find($id);

            $session->set('paymentId', $customer->getPaymentDataId());

            return $this->redirectToRoute('success');
        } catch (\Throwable $e) {
            $this->addFlash('warning', 'Could not finish registration');
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/success", name="success", methods={"GET"})
     */
    public function success(SessionInterface $session): Response
    {
        if (!$session->has('paymentId')) {
            return $this->redirectToRoute('home');
        }


        return $this->render(
            'app/success.html.twig',
            [
                'paymentId' => $session->get('paymentId')
            ]
        );
    }
}
