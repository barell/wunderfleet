<?php

declare(strict_types=1);

namespace App\Handler;

use App\Api\PaymentApiInterface;
use App\Command\RegistrationCommand;
use App\Entity\Customer;
use Doctrine\ORM\EntityManagerInterface;

final class RegistrationHandler
{
    private EntityManagerInterface $entityManager;
    private PaymentApiInterface $paymentApi;

    public function __construct(EntityManagerInterface $entityManager, PaymentApiInterface $paymentApi)
    {
        $this->entityManager = $entityManager;
        $this->paymentApi = $paymentApi;
    }

    public function handles(): string
    {
        return RegistrationCommand::class;
    }

    public function handle(RegistrationCommand $command): int
    {
        $customer = $this->createCustomer($command);

        $paymentDataId = $this->paymentApi->savePaymentData(
            $customer->getId(),
            $customer->getIban(),
            $customer->getAccountOwner()
        );

        $customer->setPaymentDataId($paymentDataId);

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer->getId();
    }

    private function createCustomer(RegistrationCommand $command): Customer
    {
        $customer = new Customer();
        $customer->setFirstName($command->getFirstName())
            ->setLastName($command->getLastName())
            ->setTelephone($command->getTelephone())
            ->setStreet($command->getStreet())
            ->setHouseNumber($command->getHouseNumber())
            ->setZipCode($command->getZipCode())
            ->setCity($command->getCity())
            ->setAccountOwner($command->getOwnerName())
            ->setIban($command->getIban());

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer;
    }
}
