<?php

declare(strict_types=1);

namespace App\Command;

use Webmozart\Assert\Assert;

final class RegistrationCommand
{
    private string $firstName;
    private string $lastName;
    private string $telephone;
    private string $street;
    private string $houseNumber;
    private string $zipCode;
    private string $city;
    private string $ownerName;
    private string $iban;

    public function __construct(
        string $firstName,
        string $lastName,
        string $telephone,
        string $street,
        string $houseNumber,
        string $zipCode,
        string $city,
        string $ownerName,
        string $iban
    ) {
        foreach (func_get_args() as $arg) {
            Assert::notEmpty($arg, \sprintf('All values in %s are required', self::class));
        }

        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->telephone = $telephone;
        $this->street = $street;
        $this->houseNumber = $houseNumber;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->ownerName = $ownerName;
        $this->iban = $iban;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->street;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): string
    {
        return $this->houseNumber;
    }

    /**
     * @return string
     */
    public function getZipCode(): string
    {
        return $this->zipCode;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getOwnerName(): string
    {
        return $this->ownerName;
    }

    /**
     * @return string
     */
    public function getIban(): string
    {
        return $this->iban;
    }
}
